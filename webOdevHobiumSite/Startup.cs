﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webOdevHobiumSite.Startup))]
namespace webOdevHobiumSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
